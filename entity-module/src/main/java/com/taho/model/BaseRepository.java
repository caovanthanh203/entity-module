package com.taho.model;

import java.util.List;

public interface BaseRepository<Model> {

    List<Model> read();

    List<Model> read(String id);

    List<Model> read(String value, String fieldName);

    Model readFirst();

    Model readFirst(String id);

    Model readFirst(String value, String fieldName);

    void insert(Model model);

    void insert(List<Model> models);

    void update(Model model);

    void update(List<Model> models);

    void delete(Model model);

    void delete(List<Model> models);

}
