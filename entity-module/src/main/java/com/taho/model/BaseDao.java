package com.taho.model;

import androidx.sqlite.db.SupportSQLiteQuery;
import java.util.List;

public interface BaseDao<Model> {

    public List<Model> read(String tableName);

    public List<Model> read(String tableName, String id);

    public List<Model> read(String tableName, String value, String fieldName);

    public Model readFirst(String tableName);

    public Model readFirst(String tableName, String id);

    public Model readFirst(String tableName, String value, String fieldName);

    public List<Model> doQuery(SupportSQLiteQuery query);

    public Model doQueryFirst(SupportSQLiteQuery query);

    public void insert(Model model);

    public void insert(List<Model> models);

    public void update(Model model);

    public void update(List<Model> models);

    public void delete(Model model);

    public void delete(List<Model> models);

}
