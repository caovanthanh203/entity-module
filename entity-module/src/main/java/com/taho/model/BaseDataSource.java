package com.taho.model;

import java.util.List;

public interface BaseDataSource<Model> {

    List<Model> read(String tableName);

    List<Model> read(String tableName, String id);

    List<Model> read(String tableName, String value, String fieldName);

    Model readFirst(String tableName);

    Model readFirst(String tableName, String id);

    Model readFirst(String tableName, String value, String fieldName);

    void insert(Model model);

    void insert(List<Model> models);

    void update(Model model);

    void update(List<Model> models);

    void delete(Model model);

    void delete(List<Model> models);

}
