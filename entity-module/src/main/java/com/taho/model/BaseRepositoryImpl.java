package com.taho.model;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

public abstract class BaseRepositoryImpl<Model> implements BaseRepository<Model> {

    private BaseDataSource<Model> defaultDataSource;

    public List<Model> read() {
        return getDefaultDataSources().read(getTableName());
    }

    public List<Model> read(String id) {
        return getDefaultDataSources().read(getTableName(), id);
    }

    public List<Model> read(String value, String fieldName) {
        return getDefaultDataSources().read(getTableName(), value, fieldName);
    }

    public Model readFirst() {
        return getDefaultDataSources().readFirst(getTableName());
    }

    public Model readFirst(String id) {
        return getDefaultDataSources().readFirst(getTableName(), id);
    }

    public Model readFirst(String value, String fieldName) {
        return getDefaultDataSources().readFirst(getTableName(), value, fieldName);
    }

    public void insert(Model model){
        getDefaultDataSources().insert(model);
    };

    public void insert(List<Model> models){
        getDefaultDataSources().insert(models);
    };

    public void update(Model model){
        getDefaultDataSources().update(model);
    };

    public void update(List<Model> models){
        getDefaultDataSources().update(models);
    };

    public void delete(Model model){
        getDefaultDataSources().delete(model);
    };

    public void delete(List<Model> models){
        getDefaultDataSources().delete(models);
    };

    protected BaseDataSource<Model> getDefaultDataSources(){
        if (null == defaultDataSource) {
            defaultDataSource = setDefaultDataSources();
        }
        return defaultDataSource;
    };

    protected abstract String getTableName();

    protected abstract <T extends BaseDataSource<Model>> T setDefaultDataSources();

}
