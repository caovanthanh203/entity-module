package com.taho.model;

import java.util.List;

public abstract class BaseDataSourceRoom<Model, Dao extends BaseDaoRoom<Model>> implements BaseDataSource<Model> {

    public List<Model> read(String tableName){
        return getDao().read(tableName);
    };

    public List<Model> read(String tableName, String id){
        return getDao().read(tableName, id);
    };

    public List<Model> read(String tableName, String value, String fieldName){
        return getDao().read(tableName, value, fieldName);
    };

    public Model readFirst(String tableName){
        return getDao().readFirst(tableName);
    };

    public Model readFirst(String tableName, String id){
        return getDao().readFirst(tableName, id);
    };

    public Model readFirst(String tableName, String value, String fieldName){
        return getDao().readFirst(tableName, value, fieldName);
    };

    public void insert(Model model){
        getDao().insert(model);
    };

    public void insert(List<Model> models){
        getDao().insert(models);
    };

    public void update(Model model){
        getDao().update(model);
    };

    public void update(List<Model> models){
        getDao().update(models);
    };

    public void delete(Model model){
        getDao().delete(model);
    };

    public void delete(List<Model> models){
        getDao().delete(models);
    };

    public abstract Dao getDao();

}
