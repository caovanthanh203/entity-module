package com.taho.model;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.List;

public abstract class BaseDaoRoom<Model> implements BaseDao<Model> {

    @RawQuery
    public List<Model> read(String tableName) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("Select * From " + tableName);
        return doQuery(query);
    }

    @RawQuery
    public List<Model> read(String tableName, String id) {
        return read(tableName, "id", id);
    }

    @RawQuery
    public List<Model> read(String tableName, String value, String fieldName) {
        String queryString = "Select * from %s where %s = '%s'";
        queryString = String.format(queryString, tableName, fieldName, value);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryString);
        return doQuery(query);
    }

    @RawQuery
    public Model readFirst(String tableName) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("Select * From " + tableName);
        return doQueryFirst(query);
    }

    @RawQuery
    public Model readFirst(String tableName, String id) {
        return readFirst(tableName, id, "id");
    }

    @RawQuery
    public Model readFirst(String tableName, String value, String fieldName) {
        String queryString = "Select * from %s where %s = '%s' limit 1";
        queryString = String.format(queryString, tableName, fieldName, value);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(queryString);
        return doQueryFirst(query);
    }

    @RawQuery
    public abstract List<Model> doQuery(SupportSQLiteQuery query);

    @RawQuery
    public abstract Model doQueryFirst(SupportSQLiteQuery query);

    @Insert
    public abstract void insert(Model model);

    @Insert
    public abstract void insert(List<Model> models);

    @Update
    public abstract void update(Model model);

    @Update
    public abstract void update(List<Model> models);

    @Delete
    public abstract void delete(Model model);

    @Delete
    public abstract void delete(List<Model> models);

}
